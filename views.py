from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from datetime import date
from googlemail.models import EMail
from mailgenerator.models import Compliment, Media, ComplimentForm, MediaForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import re

@login_required
def complimentInput(request):
    if request.method == 'POST':
        form = ComplimentForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            return home(request)
    return HttpResponseRedirect(reverse('home'))

@login_required
def mediaInput(request):
    if request.method == 'POST':
        form = MediaForm(request.POST)
        if form.is_valid():
            form.save()

    return HttpResponseRedirect(reverse('home'))


@login_required
def home(request):
    media_form = MediaForm()
    compliment_form = ComplimentForm()

    comming_media = Media.objects.filter(used = False)
    comming_compliments = Compliment.objects.filter(used = False)
    unsent_mail = EMail.objects.filter(sent=None)
    sent_mail = EMail.objects.exclude(sent=None).order_by('-sent')

    context = {'comming_media': comming_media,
               'comming_compliments': comming_compliments,
               'unsent_mail': unsent_mail,
               'sent_mail': sent_mail,
               'media_form': media_form,
               'compliment_form': compliment_form}
    

    return render_to_response('index.html',context, RequestContext(request))
               
