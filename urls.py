from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.core.urlresolvers import reverse
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$',
        'kristina.views.home',
        name='home'),
    url(r'^mail/',
        include('googlemail.urls', namespace='mail')),
    url(r'^admin/',
        include(admin.site.urls)),
    url(r'^generate/',
        include('mailgenerator.urls', namespace='mailgenerator')),
    url(r'^input/compliment$',
        'kristina.views.complimentInput',
        name='compliment'),
    url(r'^input/media$',
        'kristina.views.mediaInput',
        name='media'),
    url(r'^accounts/login/$',
        'django.contrib.auth.views.login',
        {'template_name': 'login.html'},
        name='login'),
     url(r'^accounts/logout/$',
         'django.contrib.auth.views.logout_then_login',
         name='logout'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:

)
