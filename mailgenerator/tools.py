#-*- coding: utf-8 -*-
import urllib2
import html5lib
from lxml import etree
import re

def link_title(url):
    """Returnerar titeln på sidan som url pekar på"""
    try:
        # Fetch the title
        f_url = urllib2.urlopen(url)
        doc = html5lib.parse(f_url, treebuilder='lxml')
        root = doc.getroot()
        nsmap = {'html': "http://www.w3.org/1999/xhtml"}
        title = root.xpath('//html:title/text()', namespaces=nsmap)
        
        # Cleaning
        if title == "" or title == None or title == []:
            return url
        title = re.sub(r"\s+", " ", title[0].strip())
        
        return title
    except:
        # If anything goes wrong, return the URL.
        return url
