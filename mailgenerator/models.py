#-*- coding: utf-8 -*-
from django.db import models
from django.forms import ModelForm, Textarea
import tools

# Create your models here.
class Compliment(models.Model):
    length=100

    text = models.TextField(max_length=1000)
    used = models.BooleanField(default=False)

    def __unicode__(self):
        if len(self.text) > self.length:
            return self.text[:self.length-3] + "..."
        else:
            return self.text

class ComplimentForm(ModelForm):
    class Meta:
        model = Compliment
        fields = ('text',)
        widgets = {
            'text': Textarea(attrs={'rows': 2,
                                    'placeholder': 'Skriv nu något fint till henne',
                                    'class': 'span12',}),

           } 

class Media(models.Model):
    length=100
    title = models.TextField(max_length=1000, blank=True)
    url = models.URLField(max_length=1000, unique=True)
    used = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.title == "" or self.title == self.url:
            self.title = tools.link_title(self.url)
        super(Media, self).save(*args, **kwargs)

    def __unicode__(self):
        if self.title != "": 
            return self.title
        else:
            return self.url

class MediaForm(ModelForm):
    class Meta:
        model = Media
        fields = ("url",)
        widgets = {
            'url': Textarea(attrs={'rows': 2,
                                   'class': 'span12',
                                   'placeholder': 'Ge henne ett roligt klipp',
                                   }),
            }

