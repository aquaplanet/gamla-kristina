# -*- coding: utf-8 -*-
from mailgenerator.models import Compliment, Media
from googlemail.models import EMail
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.template import RequestContext


# Create your views here.
@login_required
def generate_mail(request):
    compliments = Compliment.objects.filter(used=False)
    medias = Media.objects.filter(used=False)

    for (compliment, media) in zip(compliments, medias):
        email = EMail(subject="Ett nytt kärleksbrev",
                      media = media,
                      compliment = compliment,
                      todays_message = "")

        compliment.used = True
        media.used = True

        compliment.save()
        media.save()
        email.save()

    return HttpResponseRedirect(reverse('home'))
