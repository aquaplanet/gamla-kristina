#-*- coding: utf-8 -*-
from django.contrib import admin
from mailgenerator.models import *
from mailgenerator.tools import link_title

class ComplimentAdmin(admin.ModelAdmin):
    pass

class MediaAdmin(admin.ModelAdmin):
    actions = ['clear_title',
               'fetch_title']

    def clear_title(self, request, queryset):
        rows_updated = queryset.update(title="")
        self.message_user(request, "%d titlar blev rensade" % rows_updated)
    
    def fetch_title(self, request, queryset):
        successful = 0
        failed = 0
        
        for q in queryset:
            q.title = link_title(q.url)
            if q.url == q.title:
                # Failed to acquire title from the url
                failed += 1
            else:
                # Succeeded to get url
                successful += 1
                q.save()

        if failed == 0:
            self.message_user(request, "Updated %d titles" % (successful))
        else:
            self.message_user(request, "Tried to update %d title, %d failed" % \
                                  (successful+failed, failed))

admin.site.register(Compliment, ComplimentAdmin)
admin.site.register(Media, MediaAdmin)
