# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import EmailMultiAlternatives
from googlemail.models import EMail, EMailForm
from django.core.urlresolvers import reverse
import datetime
import re
from django.contrib.auth.decorators import login_required


@login_required
def send(request, id):
    e = EMail.objects.get(pk=id)

    # Code to send email
    email = EmailMultiAlternatives(e.subject,
                                   e.text_contents,
                                   e.from_addr,
                                   [e.to_addr])
    email.attach_alternative(e.html_contents, "text/html")
    email.send()

    e.sent = datetime.date.today()
    e.save()
    return HttpResponseRedirect(reverse('home'))

@login_required
def edit(request, id):
    if request.method == "POST":
        if "SaveEmailBTN" in request.POST:
            email = EMail.objects.get(pk=id)
            email_form = EMailForm(request.POST, instance=email)
            if email_form.is_valid():
                email_form.save()
        elif "SaveAndMailBTN" in request.POST:
            email = EMail.objects.get(pk=id)
            email_form = EMailForm(request.POST, instance=email)
            if email_form.is_valid():
                email_form.save()
                return send(request, id)
        return HttpResponseRedirect(reverse('home'))

    email = EMail.objects.get(pk=id)
    email_form = EMailForm(instance=email)
    context={'email_form': email_form,
             'id': id}
    return render_to_response('edit.html', context, RequestContext(request))
