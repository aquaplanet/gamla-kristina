# -*- coding: utf-8 -*-
from datetime import date
from django.db import models
from django.contrib import admin
from django.forms import ModelForm, TextInput, Textarea
import settings
from mailgenerator.models import Compliment, Media
from django.template.loader import render_to_string


# Create your models here.
class EMail(models.Model):
    from_addr = models.EmailField("Från", default=settings.EMAIL_FROM,
                                  help_text="Adress som mailet skickas från")
    to_addr = models.EmailField("Till", default=settings.EMAIL_TO,
                                help_text="Adress som mailet skall till. " \
                                    "Kommaseparera om det skall till flera " \
                                    "adresser.")
    subject = models.CharField("Ämne", max_length=100,
                               help_text="Ämnesraden")
    text_contents = models.TextField("Textinnehåll",
                                     help_text="Mailets innehåll i textformat",
                                     max_length=2000)
    html_contents = models.TextField("Formaterat innehåll",
                                     help_text="Mailets innehåll (formaterat)",
                                     max_length=2000)
    todays_message = models.TextField("Ett meddelande till Kristina",
                                      help_text="Skriv vad du vill ha sagt till Kristina",
                                      max_length=1000, blank=True)
    media = models.OneToOneField(Media)
    compliment = models.OneToOneField(Compliment)
    sent = models.DateField("Skickat", blank=True,  null=True)

    def __unicode__(self):
        return self.subject

    def save(self, *args, **kwargs):
        context = {'compliment': self.compliment,
                   'media': self.media,
                   'todays_message': self.todays_message,
                   }
        self.text_contents = render_to_string("mail.txt",
                                              context)
        self.html_contents = render_to_string("mail.html",
                                              context)
        if self.from_addr == self.to_addr:
            self.to_addr="kristina.olin@kentor.se"
        super(EMail, self).save(*args, **kwargs)

class EMailForm(ModelForm):
    class Meta:
        model = EMail
        fields=('from_addr','to_addr','subject','todays_message')
        widgets= {
            'from_addr': TextInput(attrs={'class': 'span8'}),
            'to_addr': TextInput(attrs={'class': 'span8'}),
            'subject': TextInput(attrs={'class': 'span8'}),
            'todays_message': Textarea(attrs={'class': 'span8'}),
            }
