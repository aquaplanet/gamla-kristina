from django.contrib import admin
from googlemail.models import EMail

class EMailAdmin(admin.ModelAdmin):
    actions = ["regenerate"]

    def regenerate(self, request, queryset):
        nr_updated = 0
        for q in queryset:
            q.save()
            nr_updated += 1
        self.message_user(request, "%d regenererade" % (nr_updated))

admin.site.register(EMail, EMailAdmin)
