from django.conf.urls.defaults import patterns, url
urlpatterns = patterns('',
                       url(r'^send/(?P<id>\d+$)', 'googlemail.views.send', name='send'),
                       url(r'^edit/(?P<id>\d+$)', 'googlemail.views.edit', name='edit'),
                       )
